import React from 'react';
import styles from './Service.module.css';

import ipad from '../../images/service-bg_ipad.png';
import web from '../../images/service-icon_web.svg';
import print from '../../images/service-icon_print.svg';
import photo from '../../images/service-icon_photo.svg';


export default function Service() {
    return(
      <div className={styles.servicewrap}>
        <div className={styles.service_grid}>
          <div className={styles.service__in}>
            <p className={styles.service__title}>OUR SERVICES</p>
             
            <div className={styles.service__block}>
              <div className={styles.service__text}>
                <span className={styles.service__descrtitle}>WEB DESIGN</span>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip sum has been the industry&#39;s standard dummy text ever
                </p>
                <div className={styles.service__iconwrap}>
                  <img src={web} alt="web icon"/>
                </div>
              </div>              
            </div>
             
            <div className={styles.service__block}>
              <div className={styles.service__text}>
                <span className={styles.service__descrtitle}>PRINT DESIGN</span>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip sum has been the industry&#39;s standard dummy text ever
                </p>
                <div className={styles.service__iconwrap}>
                  <img src={print} alt="print icon"/>
                </div>
              </div>              
            </div>

            <div className={styles.service__block}>
              <div className={styles.service__text}>
                <span className={styles.service__descrtitle}>PHOTOGRAPHY</span>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip sum has been the industry&#39;s standard dummy text ever
                </p>
                <div className={styles.service__iconwrap}>
                  <img src={photo} alt="foto icon"/>
                </div>
              </div>              
            </div>
          </div> 
          <div className={styles.service__pic}>
            <img src={ipad} alt="ipad"/>
          </div>     
        </div>
      </div>
    )
}