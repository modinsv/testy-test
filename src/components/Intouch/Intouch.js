import React from 'react';
import styles from '../Intouch/Intouch.module.css';
import Form from '../Form/Form';

export default function Intouch() {
    return(
      <div className={styles.intouch} id="contact">
        <div className={styles.intouch__wrap}>
          <div className={styles.intouch__title}>
            <span>KEEP IN TOUCH</span>
            <p>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.</p>
            <div className={styles.intouch__deviderWrapper}>
              <div className={styles.intouch__deviderWrapper__left}></div>
              <div className={styles.intouch__deviderWrapper__dot}></div>
              <div className={styles.intouch__deviderWrapper__right}></div>
            </div>
          </div> 

          <div className={styles.intouch__grid}>
            <div className={styles.intouch__blockwrap}>
              <div className={styles.intouch__address}>
                <span>OUR ADDRESS</span>
                <p>House #13, Streat road, Sydney <br/> 2310 Australia</p>
              </div>
              <div className={styles.intouch__call}>
                <span>CALL US</span>
                <p>+ 880 168 109 1425 <br/>+ 0216809142</p>
              </div>
              <div className={styles.intouch__email}>
                <span>EMAIL US</span>
                <p>contactus@email.com</p>
              </div>
            </div>
            <div className={styles.intouch__form}>
              <Form/>
            </div>
          </div>
        </div>
      </div>
    )
}