import React from 'react';
import styles from './Form.module.css';

import useInput from '../../hooks/useInput';

const isNotEmpty = value => value.trim() !== '';
const isEmail = value => value.includes('@');

export default function Form() {

  const {
    value: userNameValue,
    isValid: userNameIsValid,
    hasError: userNameHasError,
    valueChangeHandler: userNameChangeHandler,
    inputBlurHandler: userNameBlurHandler,
    reset: resetUserName,
  } = useInput(isNotEmpty);

  const {
    value: messageValue,
    isValid: messageIsValid,
    valueChangeHandler: messageChangeHandler,
    inputBlurHandler: messageBlurHandler,
    reset: resetMessage,
  } = useInput(isNotEmpty);

   const {
    value: emailValue,
    isValid: emailIsValid,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput(isEmail);

  const {
    value: subjectValue,
    isValid: subjectIsValid,
    valueChangeHandler: subjectChangeHandler,
    inputBlurHandler: subjectBlurHandler,
    reset: resetSubject,
  } = useInput(isNotEmpty);

  let formIsValid = false;

   if( userNameIsValid && messageIsValid && emailIsValid && subjectIsValid ) {
     formIsValid = true
    }
  
  //Zapier service gets info from Form and send notification to email
  //https://zapier.com/
  const zapierLink = "https://hooks.zapier.com/hooks/catch/10529379/b379c8w";

  const SendEmail = () => {
    fetch(`${zapierLink}`, {
        method: "POST",
        body: JSON.stringify({userNameValue, emailValue, messageValue, subjectValue})
      }  
    );
    resetEmail();
    resetMessage();
    resetSubject();
    resetUserName();
  }

  const handleSubmit = (event) => {
    event.preventDefault();
     
    if(!formIsValid) {
      return;
    }
    SendEmail();
  }; 

  const userNameClasses = userNameHasError ? "form__input__error" : 'form__inputs';
  const emailClasses = emailHasError ? "form__input__error" : 'form__inputs';

    return (
      <div className={styles.wrap}>
        <form className={styles.form} onSubmit={handleSubmit}>
            <input 
              type="text" 
              name="username"
              className={userNameClasses}
              placeholder="Name"
              value={userNameValue}
              onChange={userNameChangeHandler}
              onBlur={userNameBlurHandler}
            />

            <input 
              type="text" 
              name="Email"
              placeholder="Email"
              className={emailClasses}
              value={emailValue}
              onChange={emailChangeHandler}
              onBlur={emailBlurHandler}
            />

          <div className={styles.form__inputs}>
            <textarea 
              type="text" 
              name="subject"
              className={styles.form__input}
              placeholder="Subject"
              value={subjectValue}
              onChange={subjectChangeHandler}
              onBlur={subjectBlurHandler}
            />
          </div>

          <div className={styles.form__inputs}>
            <textarea 
              type="text" 
              name="message"
              className={styles.form__input}
              placeholder="Message"
              value={messageValue}
              onChange={messageChangeHandler}
              onBlur={messageBlurHandler}
            />
          </div>

          <button 
            className={styles.form__button}
            type="submit"
          >
            send message
          </button>
        </form>
      </div>
    )
}
