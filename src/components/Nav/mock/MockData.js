export const navLinks = [
    {
        id: 0,
        title: "HOME"
    },
    {
        id: 1,
        title: "ABOUT US"
    },
    {
        id: 2,
        title: "PORTFOLIO"
    },
    {
        id: 3,
        title: "PRICING"
    },
    {
        id: 4,
        title: "TEAM"
    },
    {
        id: 5,
        title: "BLOG"
    },
    {
        id: 6,
        title: "CONTACT"
    }
]