import React from 'react';
import styles from '../Nav/Nav.module.css';

import { navLinks } from './mock/MockData';

const Link = () => {
  return(
    <>
      {navLinks.map((link) => (
        <a href="/" key={link.id}>
          {link.title}
        </a>
      ))}
    </>
  )
}

export default function Nav({isExpanded, toggleExpansion}) {

    return (
      <>
        <button
          className={styles.burger}
          onClick={() => toggleExpansion(!isExpanded)}
        >
          <svg
            className={styles.burger_iconsvg}
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Меню</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
          </svg>
        </button>

        <nav className={styles.nav}>
          <Link />
        </nav>
      </>
    )
}