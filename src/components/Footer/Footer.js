import React from 'react';
import { Link } from 'react-scroll';
import styles from "./Footer.module.css";

import { details } from './mock/MockData';


const Icons = () => {
  return(
    <>
      {details.map((card) => (
        <a href="/" key={card.id}>
          <img src={card.icon} alt={card.alt}/>
        </a>
      ))}
    </>
  )
}; 

export default function Footer()  {
    return (
      <div className={styles.footerwrap}>
        <div className={styles.footer}>
          <div className={styles.footer__blockinfo}>
            <p className={styles.footer__blockinfo__title}>
              Let's Get Started Now. <span>It's FREE!</span>
            </p>
            <p className={styles.footer__blockinfo__text}>
              30 day free trial. Free plan allows up to 2 projects. Pay if you need more. Cancel anytime. 
              No catches.
            </p>
            <button className={styles.footer__blockinfo__button}>
                start free trial
            </button>
          </div>
          <div className={styles.footer__blockdown}>
            <div className={styles.footer__blockdown__social}>
              <div className={styles.footer__blockdown__svgbox}>
                <div className={styles.footer__blockdown__svgboxflex}>  
                <Icons/>
                </div>
              </div>                
              <div className={styles.footer__blockdown__rights}>
                  <p>Kazi Erfan © All Rights Reserved</p>                  
              </div>
              <Link to="top" smooth={true} duration={1500} className={styles.footer__arrow}>
              <div >
                <span>&uarr;</span> 
                </div>
              </Link>                   
            </div>              
          </div>            
        </div>           
      </div>
    )
}
