import facebook from '../../../images/footer-icon_facebook.svg';
import google from '../../../images/footer-icon_google.svg';
import linkedin from '../../../images/footer-icon_linkedin.svg';
import rss from '../../../images/footer-icon_rss.svg';
import skype from '../../../images/footer-icon_skype.svg';
import twitter from '../../../images/footer-icon_twitter.svg';

export const details = [
  {
    id: 0,
    icon: facebook,
    alt: "facebook icon"      
  },
  {
    id: 1,
    icon: google,
    alt: "google icon"      
  },
  {
    id: 2,
    icon: linkedin,
    alt: "linkedin icon"      
  },
  {
    id: 3,
    icon: rss,
    alt: "rss icon"      
  },
  {
    id: 4,
    icon: skype,
    alt: "skype icon"      
  },
  {
    id: 5,
    icon: twitter,
    alt: "twitter icon"      
  },
]