import React from 'react';
import styles from './MobileMenu.module.css';

import { navLinks } from '../Nav/mock/MockData';

const Links = () => {
  return(
    <>
      {navLinks.map((link) => (
         <a href="/" key={link.id}>
         {link.title}
       </a>
      ))}
    </>
  )
}

export default function MobileMenu({isExpanded, toggleExpansion}) {
  return (  
    <div className={styles.menu}>
      <div className={styles.menu__close}>
        <button onClick={() => toggleExpansion(!isExpanded)}>x</button>              
      </div>
      <div className={styles.menu__wrap}>             
        <div className={styles.menu__list}>
          <Links/>
        </div>
      </div>            
    </div> 
  )
}
