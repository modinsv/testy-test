import React from 'react';
import styles from './Cards.module.css';

import Card from './Card/Card';
import { cardData } from "../Cards/Card/mock/MockData";


export default function Cards() {
  return(
    <div className={styles.wrap}>
      <div className={styles.cardsGrid}>
      {cardData.map((card) => (
        <Card card={card} key={card.id}/>  
      ))
     }
      </div>
    </div>
  )
};