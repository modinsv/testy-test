import React from 'react';
import styles from '../Card/Card.module.css';

export default function Card({card}) {
  return ( 
        <div className={styles.card}>
          <div className={styles.card__iconwrap}>
            <img src={card.img} alt={card.title}/>
          </div>
          <span>{card.title}</span>
          <div className={styles.card__divider}>
            <div className={styles.card__divider_line}></div>
          </div>
          <p className={styles.card__text}>
            {card.text}
          </p>
        </div>  
  )     
}
