import clean from '../../../../images/card_clean.svg';
import creative from '../../../../images/card_creative.svg';
import sleek from '../../../../images/card_sleek.svg';
import free from '../../../../images/card_free.svg';

export const cardData = [
  {
    id:0,
    img: sleek,
    title: "SLEEK DESIGN",
    text: "Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry."
  },
  {
    id:1,
    img: clean,
    title: "CLEAN CODE",
    text: "Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry."
  },
  {
    id:2,
    img: creative,
    title: "CREATIVE IDEAS",
    text: "Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry."
  },
  {
    id:3,
    img: free,
    title: "FREE SUPPORT",
    text: "Lorem Ipsum is simply dummy text of the printing and typesetting let. Lorem Ipsum has been the industry."
  }
]