import React, { useRef } from 'react';
import { Link } from 'react-scroll';
import styles from './Slider.module.css';

import useSlider from '../../hooks/useSlider';
import logo from '../../images/logo.png';
import lo from '../../images/DownIcon.svg';

import Nav from '../Nav/Nav';

export default function Slider({images, toggleExpansion, isExpanded}) {
  
  const slideImage = useRef(null)

  const { goToPreviousSlide, goToNextSlide } = useSlider(slideImage, images)

    return (  
      <>          
        <div className={styles.slider} ref={slideImage} id="top">
          <div className={styles.wrap}>
            <div className={styles.header}>
              <div className={styles.logo}>
                <img src={logo} alt="logo"/>
              </div>
              <Nav isExpanded={isExpanded} toggleExpansion={toggleExpansion}/>
            </div>
          </div>
          <div className={styles.cont}>
            <button onClick={goToPreviousSlide} className={styles.btnleft}>
              &lt;
            </button>
            <div className={styles.sloganWrapper}>                      
              <p>Our Clients Are Our First Priority</p>
              <h1>WELCOME TO BINO</h1>
              <div className={styles.deviderWrapper}>
                <span className={styles.deviderWrapper__left}></span>
                <span className={styles.deviderWrapper__dot}></span>
                <span className={styles.deviderWrapper__right}></span>
              </div>
              <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum 
                     has been the industry's standard dummy text ever since the 1500s, when an unknown 
                     printer took a galley of type and scrambled it to make a type specimen book.
              </span>
              <div className={styles.buttons}>
                <button href="#">get started now</button>
                <button href="#">learn more</button>
              </div> 
            </div>
            <button onClick={goToNextSlide} className={styles.btnright}>
              &gt;
            </button>          
          </div>
        </div>     
        <Link to="contact" smooth={true} duration={1500}>
          <div className={styles.icon}>
            <img src={lo} alt="down"/>
          </div>
        </Link>    
      </>
        
  );
}