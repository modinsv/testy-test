import { useState } from 'react';
import { useMediaQuery } from 'react-responsive';

import Service from './components/Service/Service';
import Slider from './components/Slider/Slider';
import Cards from './components/Cards/Cards';
import Intouch from './components/Intouch/Intouch';
import MobileMenu from './components/MobileMenu/MobileMenu';
import Footer from './components/Footer/Footer';
import { slilderImages } from './components/Slider/mock/SliderImg';

function App() {
  const [isExpanded, toggleExpansion] = useState(false);

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 1024px)'
  })

  return (
   <>
   {
   isExpanded && !isDesktopOrLaptop ? 
   <MobileMenu isExpanded={isExpanded} toggleExpansion={toggleExpansion} isDesktopOrLaptop={isDesktopOrLaptop}/> : 
   <>
   <Slider images={slilderImages} isExpanded={isExpanded} toggleExpansion={toggleExpansion}/> 
   <Cards/>
   <Service/>
   <Intouch/>
   <Footer/>
   </>
   }   
   </> 
  );
}

export default App;
