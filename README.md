# Проект Welcome to Bino

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Ссылка на проект
https://bino-test.netlify.app/
## Запустить проект
1. Clone project ### `git clone https://gitlab.com/modinsv/testy-test.git`
2. Intsall dependencies `npm i` 
3. In the project directory, you can run:`npm run start`
4. Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.\
You will also see any lint errors in the console.

## Deploy on Netlify with CI/CD pipeline Gitlab
## SVG 
react-svg ###`npm i react-svg`